class Grid {
  constructor(columns, rows, cellSize) {
    this.columns = columns;
    this.rows = rows;
    this.cellSize = cellSize
    this.boardElement = document.createElement('div')
    this.boardElement.id = 'board'
    this.cellGrid = []
    for (let columnIndex = 0; columnIndex < this.columns; columnIndex++) {
      this.columnElement = document.createElement('div')
      this.columnElement.className = 'column'
      this.cellGrid.push([])
      for (let rowIndex = 0; rowIndex < this.rows; rowIndex++) {
        let newCell = new Cell(this.cellSize, columnIndex, rowIndex)
        this.cellGrid[columnIndex].push(newCell)
        this.columnElement.appendChild(newCell.cellElement)
      }
      this.boardElement.appendChild(this.columnElement)
    }
  }
  updateDOM(destination, element) {
    destination.appendChild(element)
  }
  returnCell(columnIndex, rowIndex){
    return this.cellGrid[columnIndex][rowIndex]
  }
  returnNeighbors(columnIndex, rowIndex){
    this.cellGrid[columnIndex, rowIndex]
    let neighborsArray = []
    neighborsArray.push(this.cellGrid[columnIndex+1][rowIndex])
    neighborsArray.push(this.cellGrid[columnIndex-1][rowIndex])
    neighborsArray.push(this.cellGrid[columnIndex][rowIndex+1])
    neighborsArray.push(this.cellGrid[columnIndex][rowIndex-1])
    return neighborsArray
    
  }
}

class Cell {
  constructor(size, columnIndex, rowIndex){
    this.height = size
    this.width = size
    this.columnIndex = columnIndex
    this.rowIndex = rowIndex
    this.cellElement = document.createElement('div')
    this.cellElement.className = 'cell'
    this.cellElement.id = columnIndex + ' ' + rowIndex
    this.cellElement.style.height = this.height + 'px'
    this.cellElement.style.width = this.width + 'px'
    this.cellElement.style.border = '1px solid'
  }
  swapStyles(newClass){
    this.cellElement.className = newClass
  }
}
const newGrid = new Grid(5, 5, 50);
newGrid.updateDOM(document.body,newGrid.boardElement)
console.log(newGrid.returnCell(0,3))
let neighborArray = newGrid.returnNeighbors(2,3)
for(i in neighborArray){
   neighborArray[i].swapStyles('new')
}

console.log(newGrid);